<?php
/**
 * Created by PhpStorm.
 * User: seppesnoeck
 * Date: 21/05/16
 * Time: 15:40
 */

namespace App\Http\Controllers;

use App\Locations;
use App\Reviews;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class EnrouteController extends Controller
{
    public function overview()
    {

        $locations = Locations::orderBy('stad', 'asc')
            ->paginate(5);
        $landen = Locations::orderBy('land', 'asc')
            ->select('land')
            ->get();
        return view('overview', [
            'locations' => $locations,
            'title' => 'Enroute',
            'landen' => $landen->unique('land')
        ]);
    }

    public function sortByCountry($land)
    {
        $locations = Locations::orderBy('stad', 'asc')
            -> where('land', '=', $land)
            ->paginate(5);
        $landen = Locations::orderBy('land', 'asc')
            ->select('land')
            ->get();
        return view('overview', [
            'title' => 'Enroute',
            'landen' => $landen->unique('land'),
            'locations' => $locations
        ]);
    }

    public function like($land, $stad, $id)
    {
        Locations::where('id', '=', $id)
            ->increment('likes','1');
        return Redirect::Back();
    }

    public function landing()
    {
        return view('landing', [
        'title' => 'Enroute'
        ]);
    }

    public function detail($land, $stad, $id)
    {
        $steden = Locations::where('id', '=', $id)
            ->get();
        $locations = Locations::orderBy('stad', 'asc')
            ->get();
        $reviews = Reviews::orderBy('datum_van_review', 'asc')
            ->get();
        return view('detail', [
            'title' => 'Enroute',
            'locations' => $locations,
            'steden' => $steden,
            'reviews' => $reviews
        ]);
    }

    public function login()
    {
        return view('auth/login', [
            'title' => 'Enroute'
        ]);
    }

    public function postReactie()
    {
        return 'seppe';
    }
}