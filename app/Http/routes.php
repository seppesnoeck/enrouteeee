<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/login', 'Auth\AuthController@getLogin');
Route::post('/login', 'Auth\AuthController@postLogin');
Route::get('/logout', 'Auth\AuthController@getLogout');

Route::get('/register', 'Auth\AuthController@getRegister');
Route::post('/register', 'Auth\AuthController@postRegister');

Route::get('/', 'EnrouteController@landing');
Route::get('/auth', 'Auth/AuthController@validator');

Route ::group(['middleware' => 'auth'], function() {
    Route::post('plaatsen', 'EnrouteController@postReactie');
    Route::get('/alle landen', 'EnrouteController@overview');
    Route::get('/{land}', 'EnrouteController@sortByCountry');
    Route::get('/{land}/{stad}/{id}/like', 'EnrouteController@like');
    Route::get('/{land}/{stad}/{id}', 'EnrouteController@detail');
});


Route::auth();
Route::get('/home', 'HomeController@index');

