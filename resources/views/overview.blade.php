@extends('layout')
@section('overview')
    <div class="container">

        <div class="row">

            <div class="col-md-3">
                <div class="list-group">
                    <a href="{{url('/alle landen')}}" class="list-group-item">Alle landen</a>
                    @if($landen)
                        @forelse($landen as $land)
                            <a href="{{url('/')}}/{{ $land->land }}" class="list-group-item">{{ $land->land }}</a>
                        @empty
                            <p>No countries found!</p>
                        @endforelse
                    @endif
                </div>
            </div>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img class="slide-image" src="{{ asset('../resources/assets/img/slideshow-1.jpg')}}" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="{{ asset('../resources/assets/img/slideshow-2.jpg')}}" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="{{ asset('../resources/assets/img/slideshow-3.jpg')}}" alt="">
                                </div>
                                <div class="item">
                                    <img class="slide-image" src="{{ asset('../resources/assets/img/slideshow-4.jpg')}}" alt="">
                                </div>
                            </div>
                            <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>

                @if($locations)
                    <?php $i = 0;?>
                    @foreach($locations as $location)
                        <div class="row location">
                            <div class="col-sm-12 col-lg-12 col-md-12">
                                <div class="thumbnail">
                                    <div id='map{{ $i }}' style='height:150px;'></div>
                                    <?php $i++; ?>
                                    <div class="caption">
                                        <h4 class="pull-right">{{ $location->prijs }}</h4>
                                        <h4>
                                            <a href="{{url('/')}}/{{ $location->land }}/{{ $location->stad }}/{{ $location->id }}" style="font-weight: 600">{{ $location->stad }}</a>
                                            <img height="10px" class="" src="{{ asset('../resources/assets/img/flags')}}/{{ $location->land }}.jpg" alt="">
                                        </h4>
                                        <p>{{ $location->adres }},</br>{{ $location->stad }}, {{ $location->provincie }}</p>
                                        @if($location->overnachting == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-231-moon.png')}}" title="Overnachten">
                                        @endif
                                        @if($location->water_tanken == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-678-open-water.png')}}" title="Water verkrijgbaar">
                                        @endif
                                        @if($location->chemisch_toilet_lozen == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-719-water-pipe.png')}}" title="Toilet lozen">
                                        @endif
                                        @if($location->stroom == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-206-electricity.png')}}" title="Stroom">
                                        @endif
                                        @if($location->toiletten == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-801-toilet.png')}}" title="Toiletten">
                                        @endif
                                        @if($location->douches == '1')
                                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-516-shower.png')}}" title="Douches">
                                        @endif
                                    </div>
                                    <div class="ratings">
                                        <a href="{{url('/home')}}/{{ $location->land }}/{{ $location->stad }}/{{ $location->id }}" class="pull-right" style="color: dodgerblue">{{ $location->reviews }} reviews</a>
                                        <a href="{{url('/home')}}/{{ $location->land }}/{{ $location->stad }}/{{ $location->id }}/like">
                                            <p class="pull-left" style="color: dodgerblue; margin-top: -3px">{{ $location->likes }}&nbsp</p>
                                            <p class="glyphicon glyphicon-thumbs-up" style="color: dodgerblue"></p>
                                        </a>
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {!! $locations->appends(Request::except('page'))->links() !!}
                @else
                    <p>No locations found!</p>
                @endif
            </div>
        </div>

    </div>


@endsection