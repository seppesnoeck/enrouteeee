@extends('layout')
@section('overview')
    <div class="container">
        @if($steden)
            @forelse($steden as $stad)
        <div class="row">
            <p name="location_id" class="hidden">{{ $stad->id }}</p>
            <div class="col-md-24">
                <div class="thumbnail">
                    <div id='map'  style='height:300px;'></div>
                    <div class="caption-full">
                        <h4 class="pull-right">{{ $stad->prijs }}</h4>
                        <h4><a href="{{url('/')}}/{{ $stad->land }}/{{ $stad->stad }}/{{ $stad->id }}">{{ $stad->stad }}</a>
                        </h4>
                        @if($stad->overnachting == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-231-moon.png')}}" title="Overnachten">
                        @endif
                        @if($stad->water_tanken == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-678-open-water.png')}}" title="Water verkrijgbaar">
                        @endif
                        @if($stad->chemisch_toilet_lozen == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-719-water-pipe.png')}}" title="Toilet lozen">
                        @endif
                        @if($stad->stroom == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-206-electricity.png')}}" title="Stroom">
                        @endif
                        @if($stad->toiletten == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-801-toilet.png')}}" title="Toiletten">
                        @endif
                        @if($stad->douches == '1')
                            <img height="15px" class="myicons" src="{{ asset('../resources/assets/img/glyphicons/glyphicons-516-shower.png')}}" title="Douches">
                        @endif
                        <p></p>
                        <p>{{ $stad->extra }}</p>
                    </div>
                    <div class="ratings">
                        <a href="{{url('/')}}/{{ $stad->land }}/{{ $stad->stad }}/{{ $stad->id }}" class="pull-right" style="color: dodgerblue">{{ $stad->reviews }} reacties</a>
                        <a href="{{url('/')}}/{{ $stad->land }}/{{ $stad->stad }}/{{ $stad->id }}/like">
                            <p class="pull-left" style="color: dodgerblue">{{ $stad->likes }}&nbsp</p>
                            <p class="glyphicon glyphicon-thumbs-up" style="color: dodgerblue"></p>
                        </a>
                    </div>
                </div>

                <div class="well">

                    <div class="text-right">
                        <a class="btn btn-success"id="open-review-box">Laat een reactie achter</a>
                    </div>
                    <div class="row" id="post-review-box" style="display:none;">
                        <div class="col-md-12">
                            <form accept-charset="UTF-8" action="plaatsen" method="post">
                                <input id="ratings-hidden" name="rating" type="hidden">
                                <textarea class="form-control animated" cols="50" id="new-review" name="comment" placeholder="Vertel ons je ervaring met deze locatie ..." rows="5"></textarea>

                                <div class="text-right" style="margin-top: 10px">
                                    <a class="btn btn-danger" href="#" id="close-review-box">
                                        <span class="glyphicon glyphicon-remove"></span>Annuleren
                                    </a>
                                    <button value="plaatsen" class="btn btn-success" type="submit">Plaatsen</button>
                                </div>
                            </form>
                        </div>
                    </div>

                        @foreach($reviews as $review)
                    @if($review->location_id == $stad->id)
                                <hr>

                                <div class="row">
                                    <div class="col-md-12">
                                        {{ $review->user }}
                                        <span class="pull-right">{{ date('d F Y', strtotime($review->datum_van_review)) }}</span>
                                        <p>{{ $review->inhoud }}</p>
                                    </div>
                                </div>
                        @else
                            <hr>
                            <p>Deze locatie heeft nog geen reacties!</p>
                        @endif
                    @endforeach
                </div>

            </div>

        </div>
    </div>

    <script type='text/javascript'>
        var stad = <?php echo json_encode($stad); ?>;
        function initMap(){
                var lat = stad['lat'];
                var long = stad['long'];
                var myOptions = {
                    zoom:15,
                    center:new google.maps.LatLng(lat,long),
                    zoomControl: false,
                    scaleControl: false,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP};

                map = new google.maps.Map(document.getElementById('map'), myOptions);
                marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(lat,long)});
                google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>
    @empty
        <p>No city found!</p>
    @endforelse
    @endif
@endsection