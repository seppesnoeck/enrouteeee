<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $title }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('../resources/assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('../resources/assets/css/stylish-portfolio.css')}}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>

<body>
        <!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h1>Enroute</h1>
        <h3>Find locations to sleep and park in a jiffy!</h3>
        <br>
        <a href="{{ url('/login') }}" class="btn btn-dark btn-lg">login</a>
        <a href="{{ url('/register') }}" class="btn btn-dark btn-lg">Register</a>
    </div>
</header>
</body>

</html>