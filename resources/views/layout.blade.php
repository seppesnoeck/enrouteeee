<?php
/**
 * Created by PhpStorm.
 * User: seppesnoeck
 * Date: 21/05/16
 * Time: 13:25
 */

$o =0;
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $title }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('../resources/assets/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('../resources/assets/css/main.css')}}" rel="stylesheet">
    <link href="{{ asset('../resources/assets/css/stylish-portfolio.css')}}" rel="stylesheet">
    <link href="{{ asset('../resources/assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ asset('../resources/assets/css/enroute.css')}}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top navbar-img" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{url('/alle landen')}}">Enroute</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                <!-- Right Side Of Navbar -->
                <ul class="pull-right nav navbar-nav">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}">Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div>
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/alle landen')}}">Enroute</a>
            </li>
            @for($i = 1; $i <= count(Request::segments()); $i++)
                <li>
                    <a href="">{!! Request::segment($i) !!}</a>
                    @if($i < count(Request::segments()) & $i > 0)
                    @endif
                </li>
            @endfor
        </ul>
    </div>
    <!-- Page Content -->
    @yield('overview')

    <!-- /.container -->
    <div class="container">

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>&copy; Made with &hearts; by Snoeck Seppe - Enroute 2016</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="{{ asset('../resources/assets/js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('../resources/assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('../resources/assets/js/myscripts.js')}}"></script>


    <script type='text/javascript'>
        var count = $('.location').length;
        var locations = <?php echo json_encode($locations); ?>;
        function initMap(){
            for (i = 0; i < count; i++){
                var lat = locations['data'][i]['lat'];
                var long = locations['data'][i]['long'];
            var myOptions = {
                zoom:15,
                center:new google.maps.LatLng(lat,long),
                zoomControl: false,
                scaleControl: false,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP};

                map = new google.maps.Map(document.getElementById('map'+i), myOptions);
                marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(lat,long)});
                google.maps.event.addListener(marker, 'click', function(){infowindow.open(map,marker);});
            }
        }
        google.maps.event.addDomListener(window, 'load', initMap);
    </script>

</body>

</html>