<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('land');
            $table->string('provincie');
            $table->string('adres');
            $table->string('stad');
            $table->string('gps');
            $table->string('lat');
            $table->string('long');
            $table->string('prijs');
            $table->string('aantal_plaatsen');
            $table->string('overnachting');
            $table->string('water_tanken');
            $table->string('chemisch_toilet_lozen');
            $table->string('stroom');
            $table->string('toiletten');
            $table->string('douches');
            $table->string('likes');
            $table->string('reviews');
            $table->string('extra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }
}
