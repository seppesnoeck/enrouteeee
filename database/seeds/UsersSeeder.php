<?php
/**
 * Created by PhpStorm.
 * User: seppesnoeck
 * Date: 21/05/16
 * Time: 14:16
 */


use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            'id' => '1',
            'name' => 'Seppe Snoeck',
            'email' => 'seppe@enroute.be',
            'password' => bcrypt('Azerty123'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}