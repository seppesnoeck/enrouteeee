<?php
/**
 * Created by PhpStorm.
 * User: seppesnoeck
 * Date: 21/05/16
 * Time: 14:17
 */

use Illuminate\Database\Seeder;

class ReviewsSeeder extends Seeder
{
    public function run()
    {
        DB::table('reviews')->insert([
            'user' => 'Seppe Snoeck',
            'location_id' => '11',
            'inhoud' => 'test',
            'datum_van_review' => date('d-m-Y'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}