<?php
/**
 * Created by PhpStorm.
 * User: seppesnoeck
 * Date: 21/05/16
 * Time: 14:15
 */

use Illuminate\Database\Seeder;

class LocationsSeeder extends Seeder
{
    public function run()
    {
        DB::table('locations')->insert([
            'land' => 'België',
            'provincie' => 'West-Vlaanderen',
            'adres' => 'O.L. Vrouwenmarkt',
            'stad' => 'Roeselare',
            'gps' => '50°56\'52"N 3°8\'13"E',
            'lat' => '50.9477',
            'long' => '3.1369',
            'prijs' => 'Gratis',
            'aantal_plaatsen' => '4',
            'overnachting' => '1',
            'water_tanken' => '0',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Maximaal 1 nacht overnachten.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Nederland',
            'provincie' => 'Noord-Brabant',
            'adres' => 'Katjeskelder 1',
            'stad' => 'Oosterhout',
            'gps' => '51°37\'49"N 4°49\'13"E',
            'lat' => '51.6302',
            'long' => '4.8202',
            'prijs' => 'Niet gekent',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '0',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Voor meer info surf naar www.vvvoosterhout.nl.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Nederland',
            'provincie' => 'Zeeland',
            'adres' => 'Voorstraat 47',
            'stad' => 'Groede',
            'gps' => '51°22\'57"N 3°30\'58"E',
            'lat' => '51.3825',
            'long' => '3.5161',
            'prijs' => '€5,-',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '1',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Periode: 01/04 - 01/10. Aankomst na 17 uur. Vertrek voor 10uur.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Nederland',
            'provincie' => 'Zeeland',
            'adres' => 'Veerse Meer',
            'stad' => 'Wolphaartsdijk',
            'gps' => '51°32\'35"N 3°48\'45"E',
            'lat' => '51.5430',
            'long' => '3.8125',
            'prijs' => '€14,-',
            'aantal_plaatsen' => '5',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '1',
            'toiletten' => '1',
            'douches' => '1',
            'likes' => '0',
            'reviews' => '0',
            'extra' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'België',
            'provincie' => 'Antwerpen',
            'adres' => 'Parking voor zwembad',
            'stad' => 'Arendonk',
            'gps' => '51°19\'25"N 5°4\'59"E',
            'lat' => '51.3236',
            'long' => '5.0830',
            'prijs' => 'Gratis',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Enkel water verkrijgbaar tijdens de openingsuren van het zwembad',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Frankrijk',
            'provincie' => 'Poitou',
            'adres' => 'Avenue du Port 80',
            'stad' => 'Meschers-sur-Gironde',
            'gps' => '45°33\'13"N 0°56\'40"W',
            'lat' => '45.5536',
            'long' => '-0.9444',
            'prijs' => '€7,-',
            'aantal_plaatsen' => '10',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '1',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Stroom kost €2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Frankrijk',
            'provincie' => 'Rhône - Alpes',
            'adres' => 'Aiguille du Midi',
            'stad' => 'Mont Blanc',
            'gps' => '45°55\'9"N 6°52\'8"E',
            'lat' => '45.9191',
            'long' => '6.8688',
            'prijs' => '€10,-',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '1',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Stroom kost €2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('locations')->insert([
            'land' => 'Frankrijk',
            'provincie' => 'Aquitaine',
            'adres' => 'Pont Charles de Gaulle',
            'stad' => 'St. Jean-de-Luz',
            'gps' => '43°23\'6"N 1°39\'46"E',
            'lat' => '43.1063',
            'long' => '1.6627',
            'prijs' => 'Gratis,-',
            'aantal_plaatsen' => '5',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Maximum 48 uren.',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Spanje',
            'provincie' => 'Andalusië',
            'adres' => 'Calle Marqués de Paradas',
            'stad' => 'Sevilla',
            'gps' => '37°23\'30"N 6°0\'6"W',
            'lat' => '37.3916',
            'long' => '-6.0016',
            'prijs' => '€15,-/Nacht',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '0',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Parking nabij treinstation. Ondergrond: asfalt',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Portugal',
            'provincie' => 'Lissabon',
            'adres' => 'Ten zw. van Sintra (naast vuurtoren)',
            'stad' => 'Cabo da Roca',
            'gps' => '38°46\'33"N 9°28\'36"W',
            'lat' => '38.7758',
            'long' => '-9.4766',
            'prijs' => 'Gratis',
            'aantal_plaatsen' => 'Niet gekent',
            'overnachting' => '1',
            'water_tanken' => '0',
            'chemisch_toilet_lozen' => '1',
            'stroom' => '0',
            'toiletten' => '0',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Het meest westelijk punt van Europa, tegenover restaurant Refugio Cabo da Roca',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Duitsland',
            'provincie' => 'Berlijn',
            'adres' => 'Senftenberger strasse 11',
            'stad' => 'Altdöbern',
            'gps' => '51°38\'17"N 14°1\'56"E',
            'lat' => '51.3680',
            'long' => '14.0322',
            'prijs' => '€10,-',
            'aantal_plaatsen' => '20',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '1',
            'toiletten' => '1',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => '',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        DB::table('locations')->insert([
            'land' => 'Duitsland',
            'provincie' => 'Sachen',
            'adres' => 'Mühlstrasse 3',
            'stad' => 'Bautzen',
            'gps' => '51°10\'51"N 14°25\'13"E',
            'lat' => '51.1808',
            'long' => '14.4202',
            'prijs' => '€8,-',
            'aantal_plaatsen' => '7',
            'overnachting' => '1',
            'water_tanken' => '1',
            'chemisch_toilet_lozen' => '0',
            'stroom' => '1',
            'toiletten' => '1',
            'douches' => '0',
            'likes' => '0',
            'reviews' => '0',
            'extra' => 'Stoom: €2-/Nacht. Maximaal 10 dagen. Aanmelden tussen 18uur - 19uur',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);
    }
}
